module.exports = {
  platform: 'gitlab',
  endpoint: 'https://gitlab.com/api/v4/',
  token: process.env.RENOVATE_TOKEN,

  //Sets Repositories to watch
  repositories: [
    'solidnerd/renovate-docker-app',
  ],
  hostRules: [
    {
      hostType: 'docker',
      hostName: proccess.env.CI_REGISTRY,
      username: process.env.CI_REGISTRY_USERNAME,
      password: proccess.env.CI_REGISTRY_PASSWORD,
    },
  ],

  logLevel: 'debug',
  // Set renovate.json as requirement
  requireConfig: true,
  // Will create an onboarding mr 
  // https://gitlab.com/solidnerd/renovate-docker-app/-/merge_requests/1
  onboarding: true,

  onboardingConfig: {
    extends: ['config:base'],
    prConcurrentLimit: 5,
  },

  // Define which managers or dep checkers should be used
  enabledManagers: [
    'dockerfile',
  ],
};
