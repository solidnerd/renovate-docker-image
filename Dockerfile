FROM node:12

ENV APP_HOME  /usr/src/app
WORKDIR $APP_HOME

# install dependencies
COPY package.json .
RUN npm install

COPY config.js .

CMD [ "npm", "run","renovate" ]
